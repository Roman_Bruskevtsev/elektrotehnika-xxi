<?php 
$background = get_field('objects_banner', 'option') ? ' style="background-image: url('.get_field('objects_banner', 'option')['url'].')"' : '';
?>
<section class="objects__banner"<?php echo $background; ?>>
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 data-aos="fade-left" data-aos-duration="1000"><?php the_field('objects_title', 'option'); ?></h1>
				</div>
			</div>
		</div>
	</div>
</section>