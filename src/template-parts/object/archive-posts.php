<?php 
$args = array(
	'posts_per_page'	=> 6,
	'post_type' 		=> 'object',
	'post_status'		=> 'publish'
);
$query = new WP_Query( $args );
$max_pages = (int) $query->max_num_pages;

$elektrotehnika = new Elektrotehnika();

if ( $query->have_posts() ) : ?>
<section class="objects__posts">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="objects__wrapper">
					<div class="row">
						<?php while ( $query->have_posts() ) { $query->the_post();
							echo $elektrotehnika->load_object_block( get_the_id() );
						} ?>
					</div>
				</div>
			</div>
		</div>
		<?php if ( $max_pages > 1) { ?>
		<div class="row">
			<div class="col-lg-12">
                <div class="show__more text-center" data-aos="fade-up" data-aos-duration="1000">
                    <button class="btn btn__black load__products" onclick="elektrotehnika.loadObjects(<?php echo $max_pages; ?>, this);"><span><?php _e('Show more', 'elektrotehnika'); ?></span></button>
                </div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>
<?php endif; 

wp_reset_postdata(); ?>