<?php
$elektrotehnika_xxi = new Elektrotehnika();

$args = array(
	'taxonomy'		=> 'service-category',
	'hide_empty'	=> true
);
$terms = get_terms($args); ?>

<section class="service__categories">
	<div class="container">
		<div class="row">
		<?php foreach ($terms as $term) { ?>
			<div class="col-md-6 col-lg-3"><?php echo $elektrotehnika_xxi->service_block($term->term_id); ?></div>
		<?php } ?>
		</div>
	</div>
</section>