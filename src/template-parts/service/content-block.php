<?php 
$background = ( get_the_post_thumbnail( $id ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( $id, 'service-thumbnail' ).');"' : '';
?>
<div class="col-lg-4">
	<a class="service" data-aos="fade-up" data-aos-duration="1000" href="<?php the_permalink(); ?>"<?php echo $background; ?>>
		<h4><b><?php the_title(); ?></b></h4>
	</a>
</div>