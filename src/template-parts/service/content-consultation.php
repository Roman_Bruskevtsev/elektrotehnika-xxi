<?php
$consultation = get_field('consultation_section', 'option');
$background = $consultation['background'] ? ' style="background-image: url('.$consultation['background']['url'].');"' : '';
if( $consultation ) { ?>
<section class="contact__us"<?php echo $background; ?>>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
			<?php if( $consultation['title'] ) { ?>
				<div class="title" data-aos="fade-left" data-aos-duration="1000"><h2><?php echo $consultation['title']; ?></h2></div>
			<?php } ?>
			<?php if( $consultation['text'] ) { ?>
				<div class="text" data-aos="fade-up" data-aos-duration="1000"><?php echo $consultation['text']; ?></div>
			<?php } ?>
			<?php if( $consultation['form_shortcode'] ) { ?>
				<div class="form__row" data-aos="fade-up" data-aos-duration="1000">
					<?php echo do_shortcode( $consultation['form_shortcode'] ); ?>
				</div>
			<?php } ?>
			</div>
		</div>
	</div>
</section>
<?php } 