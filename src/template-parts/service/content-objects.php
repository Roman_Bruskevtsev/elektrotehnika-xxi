<?php
$objects = get_field('objects_slider', 'option');
$elektrotehnika_xxi = new Elektrotehnika();
if( $objects ) { ?>
<section class="objects__section">
	<div class="container">
		<?php if( $objects['title'] ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="title" data-aos="fade-left" data-aos-duration="1000">
					<h2><?php echo $objects['title']; ?></h2>
					<div class="objects__nav">
						<div class="slick-prev"></div>
						<div class="slick-next"></div>
					</div>
				</div>
			</div>
		</div>
		<?php } 
		if( $objects['choose_objects'] ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="objects__slider">
				<?php foreach ( $objects['choose_objects'] as $object ) { ?>
					<div class="slide">
						<?php echo $elektrotehnika_xxi->object_block( $object->ID ); ?>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
		<?php } 
		if( $objects['objects_link'] ) { 
			$target = $objects['objects_link']['target'] ? ' target="'.$objects['objects_link']['target'].'"' : '';
		?>
		<div class="row">
			<div class="col-lg-12">
				<div class="btn__row text-center">
					<a class="btn btn__black" href="<?php echo $objects['objects_link']['url']; ?>"<?php echo $target; ?> data-aos="fade-up" data-aos-duration="1000"><?php echo $objects['objects_link']['title']; ?></a>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>
<?php } ?>