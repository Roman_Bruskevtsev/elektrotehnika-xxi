<?php
$banner = get_field('banner');
$background = $banner ? ' style="background-image: url('.$banner['url'].');"' : '';
?>
<section class="page__banner"<?php echo $background; ?>>
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 data-aos="fade-left" data-aos-duration="1000"><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
</section>