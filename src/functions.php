<?php
/**
 *
 * @package WordPress
 * @subpackage Elektrotehnika
 * @since 1.0
 */

/*ACF Import*/
require get_template_directory() . '/inc/acf-import.php';

/*Elektrotehnika*/
require get_template_directory() . '/inc/classes/elektrotehnika.php';