<?php 
$slider = get_sub_field('slider');
?>
<section class="home__banner">
<?php if($slider) { ?>
	<div class="banner__slider">
	<?php foreach ($slider as $slide) { 
		$background = $slide['background'] ? ' style="background-image: url('.$slide['background']['url'].');"' : ''; 
		$target = $slide['link']['target'] ? ' target="'.$slide['link']['target'].'"' : ''; ?>
		<div class="slide"<?php echo $background; ?>>
			<div class="content">
				<div class="container">
					<div class="row">
						<div class="col">
							<?php echo $slide['content']; ?>
							<?php if( $slide['link'] ) { ?>
								<a class="btn btn__black" href="<?php echo $slide['link']['url']; ?>"<?php echo $target; ?>><?php echo $slide['link']['title'] ?></a>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
	</div>
<?php } ?>	
</section>