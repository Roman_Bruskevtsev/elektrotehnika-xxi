<?php 
$choose_content = get_sub_field('choose_content');
$class = $choose_content == 1 ? ' noimage' : '';
?>
<section class="contacts<?php echo $class; ?>">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<?php if( get_sub_field('title') ) { ?>
				<div class="title" data-aos="fade-right" data-aos-duration="1000">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
				<?php } ?>
				<?php if( get_sub_field('text') ) { ?>
				<div class="text" data-aos="fade-right" data-aos-duration="1000"><?php the_sub_field('text'); ?></div>
				<?php } ?>
			</div>
			<?php if( $choose_content == 1 ) { 
				$form = get_sub_field('form_shortcode'); 
				if( $form ) { ?>
			<div class="col-lg-2"></div>
			<div class="col-lg-6">
				<div class="form__block" data-aos="fade-left" data-aos-duration="1000">
					<?php echo do_shortcode($form); ?>
				</div>
			</div>
				<?php } 
			} ?>
		</div>
	</div>
	<?php 
	if($choose_content == 0) {
		$background = get_sub_field('image');

		if( $background ) { ?>
			<div class="image" data-aos="fade-left" data-aos-duration="1000" style="background-image: url(<?php echo $background['url']; ?>);"></div>
		<?php } 
	} ?>
</section>