'use strict';

class elektrotehnikaClass{
    constructor(){
    	this.maxPages = false;
    	this.currentPage = 1;
    }

    loadObjects(page, button){
    	this.maxPages = (!this.maxPages) ? page : this.maxPages;
    	let xhr       = new XMLHttpRequest(),
    		wrapper   = button.closest('.objects__posts').querySelector('.objects__wrapper .row');

    	button.classList.add('load');
		if(this.maxPages > this.currentPage){
			xhr.open('POST', ajaxurl, true)
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;');
            xhr.onload = function () {
            	if (this.status >= 200 && this.status < 400) {
            		let res  = JSON.parse(this.response),
            			html = res.data.html;
            		
            		wrapper.insertAdjacentHTML('beforeend', html);

            		button.classList.remove('load');
            		elektrotehnika.currentPage++;
            		if(elektrotehnika.maxPages < elektrotehnika.currentPage + 1) button.classList.add('disable');
            	}
            };
            xhr.onerror = function() {
                console.log('connection error');
            };
            xhr.send('action=load_objects&page=' + (this.currentPage + 1));
		}
    }
}
let elektrotehnika = new elektrotehnikaClass();