'use strict';

(function($) {
    $(window).on('load', function(){
    	$('.preloader__wrapper').addClass('load');

    	/*Mobile menu*/
    	$('.mobile__btn').on('click', function(){
    		$(this).toggleClass('show');
    		$('.menu__block').toggleClass('show');
    	});
      	/*Home slider*/
      	if($('.banner__slider').length){
	        $('.banner__slider').slick({
	            infinite:       false,
	            autoplay:       true,
	            autoplaySpeed:  10000,
	            speed:          1200,
	            arrows:         true
	        });
		}

		/*About slider*/
		if($('.about__us .slider').length){
	        $('.about__us .slider').slick({
	            infinite:       false,
	            autoplay:       true,
	            autoplaySpeed:  10000,
	            speed:          1000,
	            arrows:         true
	        });
		}

		/*Objects slider*/
		if($('.objects__slider').length){
	        $('.objects__slider').slick({
	            infinite:       false,
	            autoplay:       true,
	            autoplaySpeed:  10000,
	            speed:          1000,
	            arrows:         true,
	            slidesToShow: 	2,
  				slidesToScroll: 1,
  				prevArrow:   	$('.objects__nav .slick-prev'),
  				nextArrow:		$('.objects__nav .slick-next'),
  				responsive:[
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
	        });
		}

		/*Partners slider*/
		if($('.partners').length){
	        $('.partners').slick({
	            infinite:       true,
	            autoplay:       true,
	            autoplaySpeed:  10000,
	            speed:          1000,
	            slidesToShow: 	6,
  				slidesToScroll: 1,
	            prevArrow:   	$('.partners__nav .slick-prev'),
  				nextArrow:		$('.partners__nav .slick-next'),
  				responsive:[
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 576,
                        settings: {
                            slidesToShow: 2
                        }
                    }
                ]
	        });
		}

		/*Objects*/
		$('.object__gallery').lightGallery({
			selector: 	'.object__image',
			thumbnail: 	false
		}); 

    	AOS.init();
    });
})(jQuery);