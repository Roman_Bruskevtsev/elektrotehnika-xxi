<?php
/**
 *
 * @package WordPress
 * @subpackage Elektrotehnika
 * @since 1.0
 * @version 1.0
 */
get_header(); 

get_template_part( 'template-parts/service/content', 'banner' );
get_template_part( 'template-parts/page/breadcrumb' );

get_footer();